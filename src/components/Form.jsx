import React, { Component } from 'react'
import './Form.css'
import { connect } from "react-redux";
import * as actions from '../actions/formaction'; 
import { bindActionCreators } from "redux";

class Form extends Component {

  
    state = {
        ...this.returnStateObject()
    }

    returnStateObject() {
        if (this.props.currentIndex === -1)
            return {
                CompanyName: '',
                address: '',
                employees: '',
                international: ''
            }
        else
            return this.props.list[this.props.currentIndex]
    }

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    componentDidUpdate(prevProps) {
        if (prevProps.currentIndex !== this.props.currentIndex || prevProps.list.length !== this.props.list.length) {
            this.setState({ ...this.returnStateObject() })
        }
    }


    handleSubmit = (e) => {
        e.preventDefault()
        if (this.props.currentIndex === -1)
            this.props.insertData(this.state)
        else
            this.props.updateData(this.state)
    }
sankalp

    render() {
        return (
            <div>
                <form action="">
                    <input type="text" name='CompanyName' placeholder='Enter company name...' onChange={this.handleInputChange} value={this.state.CompanyName}/>
                    <input type="text" name='address' placeholder='Enter adress ...' onChange={this.handleInputChange} value={this.state.address}/>
                    <input type="text" name='employees' placeholder='Enter employee no...' onChange={this.handleInputChange}  value={this.state.employees}/>
                    <input type="text" name='international' placeholder='is it international true or false...' onChange={this.handleInputChange}  value={this.state.international}/>
                    <button >Add detail</button>
                </form>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        list: state.list,
        currentIndex: state.currentIndex
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        insertData: actions.insert,
        updateData: actions.update
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)

// export default Form
